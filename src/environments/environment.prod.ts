export const environment = {
  production: true,
  debugMode: false,
  backendHostname: 'backend-rl-dev.52.138.16.48.nip.io',
  backendPort: '8080',
  linkageHostname: 'backend-rl-dev.52.138.16.48.nip.io',
  linkagePort: '8080',
  programsHostname: 'backend-rl-dev.52.138.16.48.nip.io',
  programsPort: '8080'
};
