// *********
// For now this is the niave approach to environment configuration a much more appropriate implementation is 
//  https://jvandemo.com/how-to-use-environment-variables-to-configure-your-angular-application-without-a-rebuild/ 
// *********
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  debugMode: true,
  backendHostname: 'backend-rl-dev.52.138.16.48.nip.io',
  backendPort: '8080',
  linkageHostname: 'backend-rl-dev.52.138.16.48.nip.io',
  linkagePort: '8080',
  programsHostname: 'backend-rl-dev.52.138.16.48.nip.io',
  programsPort: '8080'

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
