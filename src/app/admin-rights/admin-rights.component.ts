import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LinkageData, Program, MetaData, LinkageType, AppComponent, KeyType } from 'src/app/app.component';
import { buildLinkagesURL, buildProgramsURL, buildLinkageTypesURL, buildKeyTypesURL } from '../util/environmentHelper';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
@Component({
  selector: 'app-admin-rights',
  templateUrl: './admin-rights.component.html',
  styleUrls: ['./admin-rights.component.css']
})
export class AdminRightsComponent implements OnInit {

  program: string="";
  keyType:string="";
  linkageType: string="";
  dataObservable : Observable<Object>;


  constructor(private httpClient: HttpClient, private data: DataService, public router:Router) { }

  ngOnInit() {
  }
    
  addKeyType(){
    var kt:KeyType= {id:-1, type:this.keyType}
    this.dataObservable = this.httpClient.post<KeyType>(buildKeyTypesURL(), JSON.stringify(kt), httpOptions);
    this.dataObservable.subscribe(id=>{
      console.log("Added KeyType: { id:"+id+", type:"+this.keyType+"}");
      this.keyType="";
    });
    
  }

  addLinkageType(){
    var lt:LinkageType={id:-1,type:this.linkageType}
    this.dataObservable = this.httpClient.post<LinkageType>(buildLinkageTypesURL(),JSON.stringify(lt), httpOptions);
    this.dataObservable.subscribe(id=>{
      console.log("Added LinkageType: { id:"+id+", type:"+this.linkageType+"}");
      this.linkageType="";
    });
    
  }

  addProgram(){
    var prog: Program = {id:0,name:this.program}
    this.dataObservable = this.httpClient.post<Program>(buildProgramsURL(), JSON.stringify(prog), httpOptions);
    this.dataObservable.subscribe(id=>{
      console.log("Added Program: { id:"+id+", name:"+this.program+"}");
      this.program="";
  });
    
  }
}
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': '*/*'
    //'Host': 'localhost:8080'
  })
};